﻿using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System;
using System.Net.Sockets;
using System.Threading;
using DotNet.Utilities;
using System.Collections.Generic;
namespace Server
{
    class Program
    {
        public static UdpClient udpserver;
        public static List<UserInfo> userinfolist;
        public static IPEndPoint remotepoint;//目标IP地址
        public static Thread listenthread;//监听线程
        public static Thread heartthread;  //心脏线程
        public static Object thislock = new Object();
        static void Main(string[] args)
        {
            userinfolist = new List<UserInfo>();  //获取所有已经登陆的用户信息
            listenthread = new Thread(new ThreadStart(run));  //开始运行线程函数
            heartthread = new Thread(new ThreadStart(run2));  //开始运行线程函数
            int port = 10086;  //指定服务器的监听端口
            udpserver = new UdpClient(port);  //建立实例
            listenthread.Start();  //开启线程
            heartthread.Start();
            while (true)
            {

            }
        }
        public static void run()
        {
            byte[] buffer;  //要传输的通信语句
            while (true)
            {
                buffer = udpserver.Receive(ref remotepoint);  //接收到一个连接到服务器的用户信息
                object msgobj = SerializeHelper.Deserialize(buffer);  //解码
                Type msgtype = msgobj.GetType();  //得到Type格式的文件
                if (msgtype == typeof(UserInfo))//如果监测到了有用户登录到了这个服务器
                {
                    UserInfo userinfo = (UserInfo)msgobj;  //用于存放当前用户的信息
                    userinfo.Address = remotepoint;  //得到当前用户的地址
                    lock(thislock)
                    {
                        userinfolist.Add(userinfo);  //将这个用户的信息存放在本地用户库之中
                    }
                    Console.WriteLine(userinfo.Name + ":" + userinfo.Address.ToString());  //输出当前连接到服务器的用户信息
                    //后几步用于将服务器的所有用户的信息打包，然后发送到与之建立连接的用户的那里
                    
                    UserList userList = new UserList();  //新建一个用户列表专用类  第0步
                    lock(thislock)
                    {
                        userList.Userinfolist = userinfolist;  //将当前的用户列表存放到类库里的用户列表类型之中  第一步
                    }
                    buffer = SerializeHelper.Serialize(userList);  //把当前的用户列表类编码成字符串  第二步
                    udpserver.Send(buffer, buffer.Length, remotepoint);  //发送给用户
                }
                if (msgobj.ToString() == "Refresh")  //刷新
                {
                    UserList userList = new UserList();  //新建一个用户列表专用类  第0步
                    lock(thislock)
                    {
                        userList.Userinfolist = userinfolist;  //将当前的用户列表存放到类库里的用户列表类型之中  第一步
                    }
                    buffer = SerializeHelper.Serialize(userList);  //把当前的用户列表类编码成字符串  第二步
                    udpserver.Send(buffer, buffer.Length, remotepoint);  //发送给用户
                }
                if (msgtype == typeof(Heart))  //心跳
                {
                    Heart heart = (Heart)msgobj;
                    lock(thislock)
                    {
                        for (int i = userinfolist.Count - 1; i >= 0; i--)
                        {
                            UserInfo user = userinfolist[i];
                            if (user.Name == heart.Name)
                                user.Heart = 5;
                        }
                    }

                }
            }
        }
        public static void run2()
        {
            while(true)
            {
                for (int i = userinfolist.Count - 1; i >= 0; i--)
                {
                    UserInfo user = userinfolist[i];
                    user.Heart--;
                    if (user.Heart < 0)
                        userinfolist.Remove(user);
                }
                Thread.Sleep(1000);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Pushbox
{
    public partial class Form_About : Form
    {
        public Form_About()
        {
            InitializeComponent();
        }

        private void Form_About_Load(object sender, EventArgs e)
        {
            label1.Text = "本游戏在传统推箱子的基础上将网络联机\r\n合作模式融入其中，并引入经验值与升级\r\n系统，丰富了玩家的游戏体验。游戏中难\r\n免存在不足之处，如发现可向作者反馈，\r\n谢谢！";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("确认清除本地的账号信息吗？清除操作无法恢复，请慎重考虑！","通知",MessageBoxButtons.OKCancel,MessageBoxIcon.Information)==DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(".//boot//local.txt");
                sw.WriteLine("无名氏");
                sw.Close();
                Form_Local local = new Form_Local();
                local.ShowDialog();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Pushbox
{
    public partial class Form_Local : Form
    {
        public Form_Local()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text=="")
            {
                MessageBox.Show("用户名不能为空，请检查！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (MessageBox.Show("是否确认所填信息？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(".//boot//local.txt");
                sw.WriteLine(textBox1.Text);
                sw.WriteLine("0");
                sw.Close();
                this.Close();
            }
        }

        private void Form_Local_Load(object sender, EventArgs e)
        {

        }
    }
}

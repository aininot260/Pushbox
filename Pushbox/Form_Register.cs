﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBOPLib;
using System.Collections;
namespace Pushbox
{
    public partial class Form_Register : Form
    {
        public Form_Register()
        {
            InitializeComponent();
        }
        DBOP Query = new DBOP();
        private void register_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("用户名和密码不能为空", "出错", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if(textBox2.Text!=textBox3.Text)
            {
                MessageBox.Show("两次输入的密码不一致", "出错", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if(!Query.Check(textBox1.Text))
                MessageBox.Show("用户名已经存在", "出错", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                string str = "select * from Pushbox";
                ArrayList val = new ArrayList();
                val.Add(textBox1.Text);
                val.Add(textBox2.Text);
                val.Add(0);
                string msg = Query.Add(str, val);
                MessageBox.Show(msg, "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }
    }
}

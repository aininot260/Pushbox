﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;
namespace Pushbox
{
    public partial class Form_Start : Form
    {
        public Form_Start()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Form_Selfgame selfgame = new Form_Selfgame();
            this.Hide();
            selfgame.ShowDialog();
            this.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form_Prepare netgame = new Form_Prepare();
            this.Hide();
            netgame.ShowDialog();
            this.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form_About about = new Form_About();
            this.Hide();
            about.ShowDialog();
            this.Show();
        }
        bool is_local()
        {
            StreamReader sr = new StreamReader(".//boot//local.txt");
            string tmp = sr.ReadLine();
            sr.Close();
            if (tmp != "无名氏")
                return true;
            return false;
        }
        public static void local_login()
        {
            StreamReader sr = new StreamReader(".//boot//local.txt");
            Myclass.name = sr.ReadLine();
            Myclass.exp = Convert.ToInt32(sr.ReadLine());
            sr.Close();
            Myclass.is_login = true;
        }
        private void start_Load(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否登录网络游戏账号？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Form_Login login = new Form_Login();
                this.Hide();
                login.ShowDialog();
                this.Show();
            }
            else
            {
                while (!is_local())  //判断是否创建了本地账户
                {
                    Form_Local local = new Form_Local();
                    this.Hide();
                    local.ShowDialog();
                    this.Show();
                }
                Myclass.is_local = true;
                Myclass.is_login = true;
            }
            if (Myclass.is_local)  //如果是本地登录的，就要调用本地登录函数
            {
                local_login();
                button2.Enabled = false;
                button2.BackgroundImage = Image.FromFile(".//images//netlock.png");
            }
            label2.Text = Myclass.name;
            label3.Text = "LV." + (Myclass.exp / 100).ToString();
            progressBar1.Value = Myclass.exp % 100;
            SoundPlayer player = new SoundPlayer(".//musics//back.wav");
            player.PlayLooping();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void timer_ends_Tick(object sender, EventArgs e)
        {
            label3.Text = "LV." + (Myclass.exp / 100).ToString();
            progressBar1.Value = Myclass.exp % 100;
        }
        private void Form_Start_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DBOPLib;
using System.Threading;
namespace Pushbox
{
    public partial class Form_Netgame : Form
    {
        public Form_Netgame()
        {
            InitializeComponent();
        }
        DBOP Query = new DBOP();
        //
        public List<List<PictureBox>> pics = new List<List<PictureBox>>();
        public static Form_Netgame form_client;
        public int n, m;
        Random rd = new Random();
        public int select;
        int cnt = 0;//这个变量存的是终点一共有几个
        public int[] ends = new int[405];
        public bool[,] is_ends = new bool[20, 20];
        public string get_ends(int x)
        {
            if (x == 1) return ".\\pics\\pic1\\end.png";
            if (x == 2) return ".\\pics\\pic2\\end.png";
            if (x == 3) return ".\\pics\\pic3\\end.png";
            else return ".\\pics\\pic4\\end.png";
        }
        public string get_turns(int x, int y)
        {
            if (x == 1)
            {
                if (y == 1) return ".\\pics\\pic1\\up.png";
                if (y == 2) return ".\\pics\\pic1\\down.png";
                if (y == 3) return ".\\pics\\pic1\\left.png";
                else return ".\\pics\\pic1\\right.png";
            }
            if (x == 2)
            {
                if (y == 1) return ".\\pics\\pic2\\up.png";
                if (y == 2) return ".\\pics\\pic2\\down.png";
                if (y == 3) return ".\\pics\\pic2\\left.png";
                else return ".\\pics\\pic2\\right.png";
            }
            if (x == 3)
            {
                if (y == 1) return ".\\pics\\pic3\\up.png";
                if (y == 2) return ".\\pics\\pic3\\down.png";
                if (y == 3) return ".\\pics\\pic3\\left.png";
                else return ".\\pics\\pic3\\right.png";
            }
            else
            {
                if (y == 1) return ".\\pics\\pic4\\up.png";
                if (y == 2) return ".\\pics\\pic4\\down.png";
                if (y == 3) return ".\\pics\\pic4\\left.png";
                else return ".\\pics\\pic4\\right.png";
            }
        }
        public string get_pngs(string s, int x)
        {
            if (x == 1)
            {
                if (s == "#") return ".\\pics\\pic1\\wall.png";
                if (s == ".") return ".\\pics\\pic1\\road.png";
                else return ".\\pics\\pic1\\box.png";
            }
            if (x == 2)
            {
                if (s == "#") return ".\\pics\\pic2\\wall.png";
                if (s == ".") return ".\\pics\\pic2\\road.png";
                else return ".\\pics\\pic2\\box.png";
            }
            if (x == 3)
            {
                if (s == "#") return ".\\pics\\pic3\\wall.png";
                if (s == ".") return ".\\pics\\pic3\\road.png";
                else return ".\\pics\\pic3\\box.png";
            }
            else
            {
                if (s == "#") return ".\\pics\\pic4\\wall.png";
                if (s == ".") return ".\\pics\\pic4\\road.png";
                else return ".\\pics\\pic4\\box.png";
            }

        }
        void swap(PictureBox x, PictureBox y)
        {
            PictureBox t = new PictureBox();
            t.Image = x.Image;
            x.Image = y.Image;
            y.Image = t.Image;
            t.Tag = x.Tag;
            x.Tag = y.Tag;
            y.Tag = t.Tag;
        }
        bool is_win()
        {
            bool flag = true;
            for (int i = 0; i < cnt; i++)
            {
                int x = ends[i] / m;
                int y = ends[i] % m;
                if ((string)(pics[x][y].Tag) == "*")
                    continue;
                else
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        public bool flag = true;  //第一个人走的的判断
        private void timer_ends_Tick(object sender, EventArgs e)
        {
            if (flag)
                if (pics[x0][y0].Tag.ToString() != "#")  //如果第一个人已经走了
                {
                    pics[x0][y0].Tag = "#";
                    pics[x0][y0].Image = Image.FromFile(get_turns(select, 1));  //把小人最开始的位置画出来
                    Form_Prepare.form_server.net_player();
                    flag = false;
                }
            if (is_win())
            {
                timer_ends.Enabled = false;
                Myclass.exp += 87;
                //UPDATE
                string Str = "Select * From Pushbox Where Name='" + Myclass.name + "'";
                Query.Update(Str, Myclass.exp);
                MessageBox.Show("胜利", "游戏提示", MessageBoxButtons.OK);
            }
            label3.Text = "LV." + (Myclass.exp / 100).ToString();
            progressBar1.Value = Myclass.exp % 100;
        }

        private void Form_Netgame_KeyDown(object sender, KeyEventArgs e)
        { 
            flag = false;
            PictureBox pic = pics[x1][y1];
            if (e.KeyValue == 37) //left
            {
                if (pics[x1][y1 - 1].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1][y1 - 1]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1, y1 - 1);
                    y1--;
                    Form_Prepare.form_server.net_dir(2, 1);
                }
                else if (pics[x1][y1 - 1].Tag.ToString() == "*" && pics[x1][y1 - 2].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1][y1 - 2]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1, y1 - 2);
                    swap(pics[x1][y1 - 2], pics[x1][y1 - 1]);
                    Form_Prepare.form_server.net_swap(x1, y1 - 2, x1, y1 - 1);
                    y1--;
                    Form_Prepare.form_server.net_dir(2, 1);
                }
                pics[x1][y1].Image = Image.FromFile(get_turns(select, 3));
                Form_Prepare.form_server.net_turns(3);
            }
            if (e.KeyValue == 39) //right
            {
                if (pics[x1][y1 + 1].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1][y1 + 1]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1, y1 + 1);
                    y1++;
                    Form_Prepare.form_server.net_dir(2, 2);
                }

                else if (pics[x1][y1 + 1].Tag.ToString() == "*" && pics[x1][y1 + 2].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1][y1 + 2]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1, y1 + 2);
                    swap(pics[x1][y1 + 2], pics[x1][y1 + 1]);
                    Form_Prepare.form_server.net_swap(x1, y1 + 2, x1, y1 + 1);
                    y1++;
                    Form_Prepare.form_server.net_dir(2, 2);
                }
                pics[x1][y1].Image = Image.FromFile(get_turns(select, 4));
                Form_Prepare.form_server.net_turns(4);
            }
            if (e.KeyValue == 38) //down
            {
                if (pics[x1 - 1][y1].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1 - 1][y1]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1 - 1, y1);
                    x1--;
                    Form_Prepare.form_server.net_dir(1, 1);
                }

                else if (pics[x1 - 1][y1].Tag.ToString() == "*" && pics[x1 - 2][y1].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1 - 2][y1]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1 - 2, y1);
                    swap(pics[x1 - 2][y1], pics[x1 - 1][y1]);
                    Form_Prepare.form_server.net_swap(x1 - 2, y1, x1 - 1, y1);
                    x1--;
                    Form_Prepare.form_server.net_dir(1, 1);
                }
                pics[x1][y1].Image = Image.FromFile(get_turns(select, 2));
                Form_Prepare.form_server.net_turns(2);
            }
            if (e.KeyValue == 40) //up
            {
                if (pics[x1 + 1][y1].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1 + 1][y1]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1 + 1, y1);
                    x1++;
                    Form_Prepare.form_server.net_dir(1, 2);
                }
                else if (pics[x1 + 1][y1].Tag.ToString() == "*" && pics[x1 + 2][y1].Tag.ToString() == ".")
                {
                    swap(pic, pics[x1 + 2][y1]);
                    Form_Prepare.form_server.net_swap(x1, y1, x1 + 2, y1);
                    swap(pics[x1 + 2][y1], pics[x1 + 1][y1]);
                    Form_Prepare.form_server.net_swap(x1 + 2, y1, x1 + 1, y1);
                    x1++;
                    Form_Prepare.form_server.net_dir(1, 2);
                }
                pics[x1][y1].Image = Image.FromFile(get_turns(select, 1));
                Form_Prepare.form_server.net_turns(1);
            }

        }

        //
        int x1, y1;  //玩家1的当前位置
        public int x2, y2;  //玩家2的当前位置

        private void Form_Netgame_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form_Prepare.form_server.Close();
        }

        private void timer_keeps_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    if (pics[i][j].Tag.ToString() == ".")
                    {
                        pics[i][j].Image = Image.FromFile(get_pngs(".", select));
                    }
                }
            for (int i = 0; i < cnt; i++)
            {
                int x = ends[i] / m;
                int y = ends[i] % m;
                if(!(x==x1&&y==y1))
                    if (!(x == x2 && y == y2))
                        if(pics[x][y].Tag.ToString()!="*")
                        pics[x][y].Image = Image.FromFile(get_ends(select));
            }

        }

        public int x0, y0; //起点坐标
        private void netgame_Load(object sender, EventArgs e)
        {
            form_client = this;
            label2.Text = Myclass.name;
            label3.Text = "LV." + (Myclass.exp / 100).ToString();
            progressBar1.Value = Myclass.exp % 100;
            label5.Text = Myclass.net_name;
            label4.Text = "LV." + (Myclass.net_exp / 100).ToString();
            progressBar2.Value = Myclass.net_exp % 100;
            string t1 = ".\\maps\\";
            string t2 = ".txt";
            int tmap = Form_Prepare.form_server.map;
            string t3 = t1 + tmap.ToString() + t2;
            StreamReader sr = new StreamReader(t3);
            string s;
            string[] tmp = new string[105];
            char[] c = { ',' };
            s = sr.ReadLine();
            tmp = s.Split(c);
            n = Convert.ToInt32(tmp[0]);
            m = Convert.ToInt32(tmp[1]);
            select = rd.Next(1, 5);
            panel1.Width = m * 35;
            panel1.Height = n * 35;
            int panelWidth = panel1.Width;
            int panelHeight = panel1.Height;
            int picWidth = panelWidth / m;
            int picHeight = panelHeight / n;
            for (int i = 0; i < n; i++)
            {
                pics.Add(new List<PictureBox>());
                for (int j = 0; j < m; j++)
                {
                    PictureBox tmpPic = new PictureBox();
                    tmpPic.Height = picHeight;
                    tmpPic.Width = picWidth;
                    tmpPic.Location = new Point(j * picWidth, i * picHeight);
                    tmpPic.SizeMode = PictureBoxSizeMode.StretchImage;
                    tmpPic.Name = (i * m + j).ToString();
                    tmpPic.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                    panel1.Controls.Add(tmpPic);
                    pics[i].Add(tmpPic);
                }
            }
            for (int i = 0; i < n; i++)  //这两重循环是用于画图片的
            {
                s = sr.ReadLine();
                for (int j = 0; j < m; j++)
                {
                    pics[i][j].Image = Image.FromFile(get_pngs(s[j].ToString(), select));
                    pics[i][j].Tag = s[j].ToString();
                }
            }
            s = sr.ReadLine();  //这一行读的是终点的坐标
            tmp = s.Split(c);
            for (int i = 0; i < tmp.Length; i++)  //这个是把终点读出来存下
            {
                int temp = Convert.ToInt32(tmp[i]);
                ends[cnt] = temp;
                cnt++;
                int x = temp / m;
                int y = temp % m;
                pics[x][y].Image = Image.FromFile(get_ends(select));
            }
            s = sr.ReadLine();  //这一行是读取起始位置
            int tp = Convert.ToInt32(s);
            x0 = tp / m;
            y0 = tp % m;
            pics[x0][y0].Image = Image.FromFile(get_turns(select, 1));  //把小人最开始的位置画出来
            pics[x0][y0].Tag = "#";  //把小人当成墙
            x1 = x0;
            x2 = x0;
            y1 = y0;
            y2 = y0;
        }
    }
}

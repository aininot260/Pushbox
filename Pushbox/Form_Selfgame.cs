﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DBOPLib;

namespace Pushbox
{
    public partial class Form_Selfgame : Form
    {
        public Form_Selfgame()
        {
            InitializeComponent();
        }
        DBOP Query = new DBOP();
        //
        List<List<PictureBox>> pics = new List<List<PictureBox>>();
        int n, m;
        Random rd = new Random();
        int select;
        int[] ends = new int[405];
        bool[,] is_ends = new bool[20, 20];
        int cnt = 0;
        int x0, y0;
        string get_ends(int x)
        {
            if (x == 1) return ".\\pics\\pic1\\end.png";
            if (x == 2) return ".\\pics\\pic2\\end.png";
            if (x == 3) return ".\\pics\\pic3\\end.png";
            else return ".\\pics\\pic4\\end.png";
        }
        string get_turns(int x, int y)
        {
            if (x == 1)
            {
                if (y == 1) return ".\\pics\\pic1\\up.png";
                if (y == 2) return ".\\pics\\pic1\\down.png";
                if (y == 3) return ".\\pics\\pic1\\left.png";
                else return ".\\pics\\pic1\\right.png";
            }
            if (x == 2)
            {
                if (y == 1) return ".\\pics\\pic2\\up.png";
                if (y == 2) return ".\\pics\\pic2\\down.png";
                if (y == 3) return ".\\pics\\pic2\\left.png";
                else return ".\\pics\\pic2\\right.png";
            }
            if (x == 3)
            {
                if (y == 1) return ".\\pics\\pic3\\up.png";
                if (y == 2) return ".\\pics\\pic3\\down.png";
                if (y == 3) return ".\\pics\\pic3\\left.png";
                else return ".\\pics\\pic3\\right.png";
            }
            else
            {
                if (y == 1) return ".\\pics\\pic4\\up.png";
                if (y == 2) return ".\\pics\\pic4\\down.png";
                if (y == 3) return ".\\pics\\pic4\\left.png";
                else return ".\\pics\\pic4\\right.png";
            }
        }
        string get_pngs(string s, int x)
        {
            if (x == 1)
            {
                if (s == "#") return ".\\pics\\pic1\\wall.png";
                if (s == ".") return ".\\pics\\pic1\\road.png";
                else return ".\\pics\\pic1\\box.png";
            }
            if (x == 2)
            {
                if (s == "#") return ".\\pics\\pic2\\wall.png";
                if (s == ".") return ".\\pics\\pic2\\road.png";
                else return ".\\pics\\pic2\\box.png";
            }
            if (x == 3)
            {
                if (s == "#") return ".\\pics\\pic3\\wall.png";
                if (s == ".") return ".\\pics\\pic3\\road.png";
                else return ".\\pics\\pic3\\box.png";
            }
            else
            {
                if (s == "#") return ".\\pics\\pic4\\wall.png";
                if (s == ".") return ".\\pics\\pic4\\road.png";
                else return ".\\pics\\pic4\\box.png";
            }

        }
        void swap(PictureBox x, PictureBox y)
        {
            PictureBox t = new PictureBox();
            t.Image = x.Image;
            x.Image = y.Image;
            y.Image = t.Image;
        }
        void real_swap(PictureBox x, PictureBox y)
        {
            PictureBox t = new PictureBox();
            t.Image = x.Image;
            x.Image = y.Image;
            y.Image = t.Image;
            t.Tag = x.Tag;
            x.Tag = y.Tag;
            y.Tag = t.Tag;
        }
        void keep_ends()
        {
            for (int i = 0; i < cnt; i++)
            {
                int x = ends[i] / m;
                int y = ends[i] % m;
                pics[x][y].Image = Image.FromFile(get_ends(select));
                if ((string)(pics[x][y].Tag) == "*")
                    pics[x][y].Image = Image.FromFile(get_pngs("*", select));
            }
        }
        void keep_roads(int x, int y)
        {
            if (!is_ends[x, y])
                pics[x][y].Image = Image.FromFile(get_pngs((string)(pics[x][y].Tag), select));
            if (sx != x && sy != y)
                pics[sx][sy].Image = Image.FromFile(get_pngs(".", select));
        }
        bool is_win()
        {
            bool flag = true;
            for (int i = 0; i < cnt; i++)
            {
                int x = ends[i] / m;
                int y = ends[i] % m;
                if ((string)(pics[x][y].Tag) == "*")
                    continue;
                else
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        private void timer_ends_Tick(object sender, EventArgs e)
        {
            if (is_win()&&!Myclass.is_local)
            {
                timer_ends.Enabled = false;
                Myclass.exp += 87;
                //UPDATE
                string Str = "Select * From Pushbox Where Name='" +Myclass.name+ "'";
                Query.Update(Str, Myclass.exp);
                MessageBox.Show("胜利", "游戏提示", MessageBoxButtons.OK);
            }
            if(is_win() && Myclass.is_local)
            {
                timer_ends.Enabled = false;
                Myclass.exp += 87;
                StreamWriter sw = new StreamWriter(".//boot//local.txt");
                sw.WriteLine(Myclass.name);
                sw.WriteLine(Myclass.exp);
                sw.Close();
                MessageBox.Show("胜利", "游戏提示", MessageBoxButtons.OK);
            }
            label3.Text = "LV." + (Myclass.exp / 100).ToString();
            progressBar1.Value = Myclass.exp % 100;
        }

        private void Form_Selfgame_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 38) //up
            {
                if ((string)(pics[x0 - 1][y0].Tag) == "*")
                {
                    if ((string)(pics[x0 - 2][y0].Tag) == ".")
                    {
                        real_swap(pics[x0 - 1][y0], pics[x0 - 2][y0]);
                        keep_roads(x0 - 1, y0);
                        swap(pics[x0][y0], pics[x0 - 1][y0]);
                        keep_roads(x0, y0);
                        x0--;
                    }
                }
                if ((string)(pics[x0 - 1][y0].Tag) == ".")
                {
                    swap(pics[x0][y0], pics[x0 - 1][y0]);
                    keep_roads(x0, y0);
                    x0--;
                }
                keep_ends();
                pics[x0][y0].Image = Image.FromFile(get_turns(select, 2));
            }
            if (e.KeyValue == 40) //down
            {
                if ((string)(pics[x0 + 1][y0].Tag) == "*")
                {
                    if ((string)(pics[x0 + 2][y0].Tag) == ".")
                    {
                        real_swap(pics[x0 + 1][y0], pics[x0 + 2][y0]);
                        keep_roads(x0 + 1, y0);
                        swap(pics[x0][y0], pics[x0 + 1][y0]);
                        keep_roads(x0, y0);
                        x0++;
                    }
                }
                if ((string)(pics[x0 + 1][y0].Tag) == ".")
                {
                    swap(pics[x0][y0], pics[x0 + 1][y0]);
                    keep_roads(x0, y0);
                    x0++;
                }
                keep_ends();
                pics[x0][y0].Image = Image.FromFile(get_turns(select, 1));
            }
            if (e.KeyValue == 37) //left
            {
                if ((string)(pics[x0][y0 - 1].Tag) == "*")
                {
                    if ((string)(pics[x0][y0 - 2].Tag) == ".")
                    {
                        real_swap(pics[x0][y0 - 1], pics[x0][y0 - 2]);
                        keep_roads(x0, y0 - 1);
                        swap(pics[x0][y0], pics[x0][y0 - 1]);
                        keep_roads(x0, y0);
                        y0--;
                    }
                }
                if ((string)(pics[x0][y0 - 1].Tag) == ".")
                {
                    swap(pics[x0][y0], pics[x0][y0 - 1]);
                    keep_roads(x0, y0);
                    y0--;
                }
                keep_ends();
                pics[x0][y0].Image = Image.FromFile(get_turns(select, 3));
            }
            if (e.KeyValue == 39) //right
            {
                if ((string)(pics[x0][y0 + 1].Tag) == "*")
                {
                    if ((string)(pics[x0][y0 + 2].Tag) == ".")
                    {
                        real_swap(pics[x0][y0 + 1], pics[x0][y0 + 2]);
                        keep_roads(x0, y0 + 1);
                        swap(pics[x0][y0], pics[x0][y0 + 1]);
                        keep_roads(x0, y0);
                        y0++;
                    }
                }
                if ((string)(pics[x0][y0 + 1].Tag) == ".")
                {
                    swap(pics[x0][y0], pics[x0][y0 + 1]);
                    keep_roads(x0, y0);
                    y0++;
                }
                keep_ends();
                pics[x0][y0].Image = Image.FromFile(get_turns(select, 4));
            }
        }

        int sx, sy;
        int map;
        //
        private void selfgame_Load(object sender, EventArgs e)
        {
            map = rd.Next(1, 3);
            label2.Text = Myclass.name;
            label3.Text = "LV." + (Myclass.exp / 100).ToString();
            progressBar1.Value = Myclass.exp % 100;
            string t1 = ".\\maps\\";
            string t2 = ".txt";
            string t3 = t1 + map.ToString() + t2;
            StreamReader sr = new StreamReader(t3);
            string s;
            string[] tmp = new string[105];
            char[] c = { ',' };
            s = sr.ReadLine();
            tmp = s.Split(c);
            n = Convert.ToInt32(tmp[0]);
            m = Convert.ToInt32(tmp[1]);
            select = rd.Next(1, 5);
            panel1.Width = m * 35;
            panel1.Height = n * 35;
            int panelWidth = panel1.Width;
            int panelHeight = panel1.Height;
            int picWidth = panelWidth / m;
            int picHeight = panelHeight / n;
            for (int i = 0; i < n; i++)
            {
                pics.Add(new List<PictureBox>());
                for (int j = 0; j < m; j++)
                {
                    PictureBox tmpPic = new PictureBox();
                    tmpPic.Height = picHeight;
                    tmpPic.Width = picWidth;
                    tmpPic.Location = new Point(j * picWidth, i * picHeight);
                    tmpPic.SizeMode = PictureBoxSizeMode.StretchImage;
                    tmpPic.Name = (i * m + j).ToString();
                    tmpPic.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                    //tmpPic.BorderStyle =BorderStyle.FixedSingle;
                    panel1.Controls.Add(tmpPic);
                    pics[i].Add(tmpPic);
                }
            }
            for (int i = 0; i < n; i++)
            {
                s = sr.ReadLine();
                for (int j = 0; j < m; j++)
                {
                    pics[i][j].Image = Image.FromFile(get_pngs(s[j].ToString(), select));
                    pics[i][j].Tag = s[j].ToString();
                }
            }
            s = sr.ReadLine();
            tmp = s.Split(c);
            for (int i = 0; i < tmp.Length; i++)
            {
                int temp = Convert.ToInt32(tmp[i]);
                ends[cnt] = temp;
                cnt++;
                int x = temp / m;
                int y = temp % m;
                pics[x][y].Image = Image.FromFile(get_ends(select));
            }
            s = sr.ReadLine();
            int tp = Convert.ToInt32(s);
            sx = tp / m;
            sy = tp % m;
            pics[sx][sy].Image = Image.FromFile(get_turns(select, 1));
            x0 = sx;
            y0 = sy;
            keep_ends();
        }
    }
}

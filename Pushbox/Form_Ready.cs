﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pushbox
{
    public partial class Form_Ready : Form
    {
        public Form_Ready()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form_Start start = new Form_Start();
            this.Hide();
            start.ShowDialog();
            this.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form_Register register = new Form_Register();
            this.Hide();
            register.ShowDialog();
            this.Show();
        }

        private void Form_Ready_Load(object sender, EventArgs e)
        {

        }

        private void Form_Ready_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using DotNet.Utilities;
using System.Threading;
namespace Pushbox
{
    public partial class Form_Prepare : Form
    {
        public Form_Prepare()
        {
            InitializeComponent();
            userinfolist = new List<UserInfo>();  //创建本地用户列表
            listenthread = new Thread(new ThreadStart(run));  //初始化线程函数
            Control.CheckForIllegalCrossThreadCalls = false;
        }
        private IPEndPoint hostpoint;//服务器主机地址
        private IPEndPoint remotepoint;//目标主机地址
        private List<UserInfo> userinfolist;//保存所有登陆用户信息
        private Thread listenthread;//监听线程
        private UdpClient udpclient;
        UserInfo user = new UserInfo();
        UserList userlist;
        IPEndPoint userpoint;  //目标主机地址
        bool flag = true;
        bool flag2 = true;
        bool flag3 = true;
        public static Form_Prepare form_server;
        Random rd = new Random();
        public int map;
        private void button1_Click(object sender, EventArgs e)
        {
            byte[] buffer = null;
            if (flag)
            {
                string serverIp = "127.0.0.1";  //要连接的服务器的IP地址
                int Port = 10086;  //负责监听的服务器的端口号
                udpclient = new UdpClient();  //新建一个连接
                remotepoint = new IPEndPoint(IPAddress.Any, 0);//提供任何端口和IP，这里不需要指定
                hostpoint = new IPEndPoint(IPAddress.Parse(serverIp), Port);//实例化主机地址
                UserInfo userinfo = new UserInfo();  //新建一个用户类型
                userinfo.Name = Myclass.name;  //要传到服务器的用户名
                userinfo.Heart = 60;
                buffer = SerializeHelper.Serialize(userinfo);  //将自己的用户信息编码
                listenthread.Start();  //这个函数是用户的干活函数
                udpclient.Send(buffer, buffer.Length, hostpoint);  //将当前的用户信息发送到服务器
                MessageBox.Show("连接服务器成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if(userinfolist!= null)
                for (int i = 0; i < userinfolist.Count; i++)
                {
                    user.Name = userinfolist[i].Name;
                    user.Address = userinfolist[i].Address;
                    userpoint = new IPEndPoint(user.Address.Address, user.Address.Port);
                    Refresh refresh = new Refresh();
                    buffer = SerializeHelper.Serialize(refresh);  //将自己的用户信息编码
                    udpclient.Send(buffer, buffer.Length, userpoint);  //将当前的用户信息发送到服务器
                }
                flag = false;
            }
            else
            {
                buffer = SerializeHelper.Serialize("Refresh");  //将自己的用户信息编码
                udpclient.Send(buffer, buffer.Length, hostpoint);  //将当前的用户信息发送到服务器
                MessageBox.Show("刷新成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (userinfolist!= null)
                    for (int i = 0; i < userinfolist.Count; i++)
                {
                    user.Name = userinfolist[i].Name;
                    user.Address = userinfolist[i].Address;
                    userpoint = new IPEndPoint(user.Address.Address, user.Address.Port);
                    Refresh refresh = new Refresh();
                    buffer = SerializeHelper.Serialize(refresh);  //将自己的用户信息编码
                    udpclient.Send(buffer, buffer.Length, userpoint);  //将当前的用户信息发送到服务器
                }

            }
        }
        private void run()  //从这里可以进行指定和哪一个表里的用户进行通信，这里没有写
        {
            byte[] buffer;
            while (true)
            {
                try
                {
                    buffer = udpclient.Receive(ref remotepoint);  //获取与之对接用户的通话字符串
                }
                catch(Exception)
                {
                    continue;
                }
                object msgobj = SerializeHelper.Deserialize(buffer);  //解码
                Type msgtype = msgobj.GetType();  //得到Type格式的文件
                                                  //以后接收的就是客户端的了
                if (msgtype == typeof(UserList))  //如果真的接收到了与之对接用户传来的消息
                {
                    userlist = (UserList)SerializeHelper.Deserialize(buffer);  //解码
                    this.userinfolist = userlist.Userinfolist;  //将传过来的类赋值到本地的类里面
                    this.Invoke(new Action(() =>
                    {
                        listBox1.Items.Clear();
                        for (int i = 0; i < userinfolist.Count; i++)
                        {
                            UserInfo user = new UserInfo();
                            user.Name = userinfolist[i].Name;
                            user.Address = userinfolist[i].Address;
                            listBox1.Items.Add(user.Name + ":" + user.Address.ToString());
                        }
                    }));
                }
                else if (msgtype == typeof(Heart))  //如果对方接受到了请求
                {
                    Heart player = (Heart)SerializeHelper.Deserialize(buffer);  //解码
                    if (flag2)
                    {
                        if (MessageBox.Show("玩家" + player.Name + "想要与你一起游戏，是否接受该请求？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                        {
                            groupBox1.Enabled = false;
                            Myclass.net_name = player.Name;
                            Myclass.net_exp = player.Exp;
                            if(player.Map!=0)
                            map = player.Map;
                            user.Address.Address = remotepoint.Address;
                            user.Address.Port = remotepoint.Port;
                            userpoint = new IPEndPoint(user.Address.Address, user.Address.Port);
                            player.Name = Myclass.name;
                            player.Exp = Myclass.exp;
                            buffer = SerializeHelper.Serialize(player);
                            udpclient.Send(buffer, buffer.Length, userpoint);   //然后传播过去一个是否打开联机对战的请求
                            flag2 = false;
                        }
                    }
                    else
                    {
                        Asking asking = new Asking();
                        asking.is_ask = true;
                        buffer = SerializeHelper.Serialize(asking);
                        udpclient.Send(buffer, buffer.Length, userpoint);   //然后传播过去一个是否打开联机对战的请求
                    }
                }
                else if (msgtype == typeof(Asking))
                {
                    Asking asking = (Asking)SerializeHelper.Deserialize(buffer);  //解码
                    if (flag3)
                    {
                        if (asking.is_ask)
                        {
                            this.Invoke(new Action(() =>
                            {
                                Form_Netgame netgame = new Form_Netgame();
                                netgame.Show();
                            }));
                            asking = new Asking();
                            asking.is_ask = true;
                            buffer = SerializeHelper.Serialize(asking);
                            udpclient.Send(buffer, buffer.Length, userpoint);   //然后传播过去一个是否打开联机对战的请求
                            flag3 = false;
                        }
                    }
                }
                else if (msgtype == typeof(Refresh))
                {
                    Refresh refresh = (Refresh)SerializeHelper.Deserialize(buffer);  //解码
                    if (refresh.is_fresh)
                        refresh.is_fresh = false;
                }
                else if (msgtype == typeof(Handler))
                {
                    Handler handler = (Handler)SerializeHelper.Deserialize(buffer);  //解码
                    if (handler.list_swap != null)
                    {
                        int x1 = handler.list_swap[0];
                        int y1 = handler.list_swap[1];
                        int x2 = handler.list_swap[2];
                        int y2 = handler.list_swap[3];
                        Form_Netgame.form_client.Invoke(new Action(() =>
                        {
                            PictureBox t = new PictureBox();
                            t.Image = Form_Netgame.form_client.pics[x1][y1].Image;
                            Form_Netgame.form_client.pics[x1][y1].Image = Form_Netgame.form_client.pics[x2][y2].Image;
                            Form_Netgame.form_client.pics[x2][y2].Image = t.Image;
                            t.Tag = Form_Netgame.form_client.pics[x1][y1].Tag;
                            Form_Netgame.form_client.pics[x1][y1].Tag = Form_Netgame.form_client.pics[x2][y2].Tag;
                            Form_Netgame.form_client.pics[x2][y2].Tag = t.Tag;
                        }));
                    }
                    if (handler.is_player)
                    {
                        Form_Netgame.form_client.Invoke(new Action(() =>
                        {
                            Form_Netgame.form_client.pics[Form_Netgame.form_client.x0][Form_Netgame.form_client.y0].Tag = "#";
                            Form_Netgame.form_client.pics[Form_Netgame.form_client.x0][Form_Netgame.form_client.y0].Image = Image.FromFile(Form_Netgame.form_client.get_turns(Form_Netgame.form_client.select, 1));  //把小人最开始的位置画出来
                            Form_Netgame.form_client.flag = false;
                        }));
                    }
                    if (handler.list_dir != null)
                    {
                        int m = handler.list_dir[0];
                        int n = handler.list_dir[1];
                        Form_Netgame.form_client.Invoke(new Action(() =>
                        {
                            if (m == 1)
                            {
                                if (n == 1)
                                {
                                    Form_Netgame.form_client.x2--;
                                }
                                if (n == 2)
                                {
                                    Form_Netgame.form_client.x2++;
                                }

                            }
                            if (m == 2)
                            {
                                if (n == 1)
                                {
                                    Form_Netgame.form_client.y2--;
                                }

                                if (n == 2)
                                {
                                    Form_Netgame.form_client.y2++;
                                }

                            }
                        }));
                    }
                    if (handler.turns != 0)
                    {
                        Form_Netgame.form_client.Invoke(new Action(() =>
                        {
                            Form_Netgame.form_client.pics[Form_Netgame.form_client.x2][Form_Netgame.form_client.y2].Image = Image.FromFile(Form_Netgame.form_client.get_turns(Form_Netgame.form_client.select, handler.turns));
                        }));
                    }
                }
                else
                {
                    listBox2.Items.Add("对方说：" + msgobj.ToString());
                }

            }
        }
        private void Form_Prepare_Load(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            form_server = this;
            timer1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox2.Items.Add(Myclass.name + ": " + textBox1.Text);
            byte[] buffer = SerializeHelper.Serialize(textBox1.Text);  //将自己的短消息编码
            udpclient.Send(buffer, buffer.Length, userpoint);  //将当前的用户信息发送到与之对接的用户那里
        }

        private void button3_Click(object sender, EventArgs e)
        {
            map = rd.Next(1, 2);
            if(listBox1.SelectedIndex==-1)
            {
                MessageBox.Show("请选择玩家后再进行游戏","提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int tmp = listBox1.SelectedIndex;
            if(userinfolist[tmp].Name==Myclass.name)
            {
                MessageBox.Show("请不要选择自己！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            user.Name = userinfolist[tmp].Name;
            user.Address = userinfolist[tmp].Address;
            userpoint = new IPEndPoint(user.Address.Address, user.Address.Port);
            Heart player = new Heart();
            player.Name = Myclass.name;
            player.Exp = Myclass.exp;
            player.Map = map;
            byte[] buffer = SerializeHelper.Serialize(player);  //首先我把我的信息打包发过去，我是触发者
            udpclient.Send(buffer, buffer.Length, userpoint);
            MessageBox.Show("与玩家" + userinfolist[tmp].Name + "成功发送联机请求", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            Heart heart = new Heart();
            heart.Name = Myclass.name;
            byte[] buffer = SerializeHelper.Serialize(heart);  //将自己的短消息编码
            if (!flag)
                udpclient.Send(buffer, buffer.Length, hostpoint);  //将当前的用户信息发送到与之对接的用户那里
        }

        private void Form_Prepare_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Enabled = false;
            listenthread.Abort();
            if(flag==false)
            {
                udpclient.Close();//关闭协议 
                (udpclient as IDisposable).Dispose();
                udpclient = null;
            }
        }
        public void net_swap(int x1, int y1, int x2, int y2)
        {
            Handler handler = new Handler();
            handler.list_swap = new List<int>();
            handler.list_swap.Add(x1);
            handler.list_swap.Add(y1);
            handler.list_swap.Add(x2);
            handler.list_swap.Add(y2);
            byte[] buffer;
            buffer = SerializeHelper.Serialize(handler);
            udpclient.Send(buffer, buffer.Length, userpoint);   //然后传播过去一个是否打开联机对战的请求
        }
        public void net_player()
        {
            Handler handler = new Handler();
            handler.is_player = true;
            byte[] buffer;
            buffer = SerializeHelper.Serialize(handler);
            udpclient.Send(buffer, buffer.Length, userpoint);   //然后传播过去一个是否打开联机对战的请求
        }
        public void net_dir(int m, int n)
        {
            Handler handler = new Handler();
            handler.list_dir = new List<int>();
            handler.list_dir.Add(m);
            handler.list_dir.Add(n);
            byte[] buffer;
            buffer = SerializeHelper.Serialize(handler);
            udpclient.Send(buffer, buffer.Length, userpoint);   //然后传播过去一个是否打开联机对战的请求
        }
        public void net_turns(int x)
        {
            Handler handler = new Handler();
            handler.turns = x;
            byte[] buffer;
            buffer = SerializeHelper.Serialize(handler);
            udpclient.Send(buffer, buffer.Length, userpoint);   //然后传播过去一个是否打开联机对战的请求
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;

namespace DotNet.Utilities
{
    public class SerializeHelper
    {
        public SerializeHelper()
        { }
        public static byte[] Serialize(object item)
        {
            if (item == null) return null;
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            formatter.Serialize(ms, item);
            ms.Position = 0;
            byte[] b = new byte[ms.Length];
            ms.Read(b, 0, b.Length);
            ms.Close();
            return b;
        }
        public static object Deserialize(byte[] str)
        {
            if (str.Length == 0) return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            ms.Write(str, 0, str.Length);
            ms.Position = 0;
            object n = (object)bf.Deserialize(ms);
            ms.Close();
            return n;
        }
    }
}
[Serializable]
public class UserInfo
{
    public string Name
    {
        set;
        get;
    }
    public int Exp
    {
        set;
        get;
    }
    public int Heart
    {
        set;
        get;
    }
    public IPEndPoint Address
    {
        set;
        get;
    }
}
[Serializable]
public class UserList
{
    public List<UserInfo> Userinfolist
    {
        set;
        get;
    }
}
[Serializable]
public class Heart
{
    public string Name
    {
        set;
        get;
    }
    public int Exp
    {
        set;
        get;
    }
    public int Map
    {
        set;
        get;
    }
}
[Serializable]
public class Asking
{
    public bool is_ask  //如果这个是真，那么就会打开那个窗口，反之就不会
    {
        set;
        get;
    }
}
[Serializable]
public class Refresh
{
    public bool is_fresh
    {
        set;
        get;
    }
}
[Serializable]
public class Handler
{
    public List<int> list_swap
    {
        set;
        get;
    }
    public bool is_player
    {
        set;
        get;
    }
    public List<int> list_dir
    {
        set;
        get;
    }
    public int turns
    {
        set;
        get;
    }
}
